# The CID2013 Camera Image Database #

### Description ###

The CID2013 Camera Image Database consists of real images taken by consumer cameras and mobile phones. It is developed to provide useful tool to allow researchers target more commercially relevant distortions when developing processes of objective image quality assessment algorithms.

The CID2013 database consists of 480 images captured by 79 imaging devices (mobile phones, DSC, DSLR) in six Image Sets.

The images are evaluated by 188 observers using Dynamic Reference (DR-ACR) method (see readme.pdf). A separate scale realignment ACR data consisting evaluations from 34 observers is also included that allows to combine the data from the six image sets

### Download ###

Database is password protected. If you are interesting to download the database, please contact Toni Virtanen, (firstname.lastname ( at ) helsinki.fi). You may download the database from this repository.

If you use these images in your research, we kindly ask that you follow the copyright notice and cite the following paper:

Virtanen, T., Nuutinen, M., Vaahteranoksa, M., Oittinen, P. and Häkkinen, J. CID2013: a database for evaluating no-reference image quality assessment algorithms, IEEE Transactions on Image Processing, vol. 24, no. 1, pp. 390-402, Jan. 2015.

### Documentation ###

The readme document (readme.pdf) contains setup and statistics of the database.

### Copyright ###

Copyright (c) 2014 The University of Helsinki
All rights reserved.

Permission is hereby granted, without written agreement and without license or royalty fees, to use, copy, modify, and distribute this database (the videos, the images, the results and the source files) and its documentation for any purpose, provided that the copyright notice in its entirely appear in all copies of this database, and the original source of this database,Visual Cognition research group (www.helsinki.fi/psychology/groups/visualcognition/index.htm) and the Institute of Behavioral Science (www.helsinki.fi/ibs/index.html) at the University Helsinki (www.helsinki.fi/university/), is acknowledged in any publication that reports research using this database. Individual videos and images may not be used outside the scope of this database (e.g. in marketing purposes) without prior permission.

The database and our paper are to be cited in the bibliography as:

Virtanen T., Nuutinen, M., Vaahteranoksa, M., Oittinen, P. and Häkkinen, J. The CID2013 Camera Image Database, www.helsinki.fi/psychology/groups/visualcognition/index.htm.

Virtanen, T., Nuutinen, M., Vaahteranoksa, M., Oittinen, P. and Häkkinen, J. CID2013: a database for evaluating no-reference image quality assessment algorithms, IEEE Transactions on Image Processing, vol. 24, no. 1, pp. 390-402, Jan. 2015.

LIMITATION OF LIABILITY

UNIVERSITY OF HELSINKI SHALL IN NO CASE BE LIABLE IN CONTRACT, TORT OR OTHERWISE FOR ANY LOSS OF REVENUE, PROFIT, BUSINESS OR GOODWILL OR ANY DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE COST, DAMAGES OR EXPENSE OF ANY KIND HOWEVER CAUSED OR HOWEVER ARISING UNDER OR IN CONNECTION WITH THE USE OF THIS DATABASE.

THE UNIVERSITY OF HELSINKI SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE DATABASE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF HELSINKI HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
